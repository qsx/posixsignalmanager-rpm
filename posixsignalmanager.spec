%global forgeurl https://github.com/textshell/posixsignalmanager
%global commit e9eb57330d6a9dd921e351f44790cdda38f7d15b
%forgemeta

Name: posixsignalmanager
Version: 0.3
Release: %autorelease
Summary: POSIX signal handling for Qt

License: BSL-1.0
URL: %{forgeurl}
Source0: %{forgesource}

BuildRequires: meson
BuildRequires: gcc-c++
BuildRequires: pkgconfig(catch2)
BuildRequires: pkgconfig(Qt5Core)


%description
Library safe, synchronous and asynchronous handling of POSIX signals for Qt
applications and libraries.


%package devel
Summary: Header files for the PosixSignalManager library
Requires: %{name}%{?_isa} = %{version}-%{release}

%description devel
%{summary}.


%prep
%forgeautosetup -p1
rm -f tests/catch.hpp


%build
%meson -Dsystem-catch2=enabled
%meson_build


%install
%meson_install


%check
%meson_test


%files
%doc README.md
%license COPYING
%{_libdir}/libposixsignalmanager.so.0a

%files devel
%{_libdir}/libposixsignalmanager.so
%{_libdir}/pkgconfig/PosixSignalManager.pc
%{_includedir}/PosixSignalManager.h


%changelog
%autochangelog
